package com.example.carlosvazquez.tetrisdamn.Tetris;

import com.example.carlosvazquez.tetrisdamn.framework.Game;
import com.example.carlosvazquez.tetrisdamn.framework.Graphics;
import com.example.carlosvazquez.tetrisdamn.framework.Input;
import com.example.carlosvazquez.tetrisdamn.framework.Screen;

import java.util.List;

/**
 * Created by juegos on 19/1/2017.
 */

public class MainMenu extends Screen{
    public MainMenu(Game game) {
        super(game);
    }

    public void update(float deltaTime) {
        Graphics g = game.getGraphics();
        List<Input.TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();

        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            Input.TouchEvent event = touchEvents.get(i);
            if(event.type == Input.TouchEvent.TOUCH_UP) {

                if(inBounds(event, 64, 292, 192, 42) ) {
                    game.setScreen(new GameScreen(game));
                    return;
                }
            }
        }

    }

    private boolean inBounds(Input.TouchEvent event, int x, int y, int width, int height) {
        if(event.x > x && event.x < x + width - 1 &&
                event.y > y && event.y < y + height - 1)
            return true;
        else
            return false;
    }

    public void render(float deltaTime) {
        Graphics g = game.getGraphics();

        g.drawPixmap(Assets.background, 0, 0);
        g.drawPixmap(Assets.title, 10, 100);
        g.drawPixmap(Assets.menu, 64, 292);
    }

    public void pause() {

    }

    public void resume() {

    }

    public void dispose() {

    }
}
