package com.example.carlosvazquez.tetrisdamn.framework;

public interface Audio {
    public Music newMusic(String filename);

    public Sound newSound(String filename);
}
