package com.example.carlosvazquez.tetrisdamn.Tetris;

import com.example.carlosvazquez.tetrisdamn.framework.Game;
import com.example.carlosvazquez.tetrisdamn.framework.Graphics;
import com.example.carlosvazquez.tetrisdamn.framework.Screen;

/**
 * Created by carlos.vazquez on 21/12/2016.
 */
public class LoadingScreen extends Screen {



    public LoadingScreen(Game game) {
        super(game);
    }

    @Override
    public void update(float deltaTime) {
        Graphics g = game.getGraphics();
        Assets.background = g.newPixmap("background.png", Graphics.PixmapFormat.RGB565);
        Assets.cube = g.newPixmap("cube.png",  Graphics.PixmapFormat.RGB565);
        Assets.king = g.newPixmap("King.png", Graphics.PixmapFormat.RGB565);
        Assets.lft = g.newPixmap("left.png", Graphics.PixmapFormat.RGB565);
        Assets.rght = g.newPixmap("right.png", Graphics.PixmapFormat.RGB565);
        Assets.up = g.newPixmap("up.png", Graphics.PixmapFormat.RGB565);
        Assets.noel = g.newPixmap("noel.png", Graphics.PixmapFormat.ARGB4444);
        Assets.menu = g.newPixmap("startMenu.png", Graphics.PixmapFormat.ARGB4444);
        Assets.resume = g.newPixmap("resume.png", Graphics.PixmapFormat.ARGB4444);
        Assets.quit = g.newPixmap("quit.png", Graphics.PixmapFormat.ARGB4444);
        Assets.GO = g.newPixmap("GO.png", Graphics.PixmapFormat.ARGB4444);
        Assets.pause = g.newPixmap("pause.png", Graphics.PixmapFormat.ARGB4444);
        Assets.winner = g.newPixmap("winner.png", Graphics.PixmapFormat.ARGB4444);
        Assets.title = g.newPixmap("cooltitle.png", Graphics.PixmapFormat.ARGB4444);

        game.setScreen(new MainMenu(game));
    }

    @Override
    public void render(float deltaTime) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
