package com.example.carlosvazquez.tetrisdamn.Tetris;

import com.example.carlosvazquez.tetrisdamn.framework.Pixmap;

/**
 * Created by carlos.vazquez on 21/12/2016.
 */
public class King {
    public int x;
    public int y;
    public int x2;
    public int y2;
    public int x3;
    public int y3;
    public static final int UP = 0;
    public static final int LEFT = 1;
    public static final int RIGHT = 3;
    protected Pixmap graphic;
    public Board board;


    public King(){
        y = 12;
        x = (int) Math.floor(board.WORLD_WIDTH / 2);
        y2 = 12;
        x2 = (int) Math.floor(board.WORLD_WIDTH / 4);
        y3 = 12;
        x3 = (int) Math.floor(board.WORLD_WIDTH * 0.75);
    }

    public void turnLeft(int type) {
        switch(type)
        {
            case 1:
                x--;
                break;
            case 2:
                x2--;
                break;
            case 3:
                x3--;
                break;
        }

    }

    public void turnRight(int type) {
        switch(type)
        {
            case 1:
                x++;
                break;
            case 2:
                x2++;
                break;
            case 3:
                x3++;
                break;
        }
    }
    public void goUp(int type){
        switch(type)
        {
            case 1:
                y--;
                break;
            case 2:
                y2--;
                break;
            case 3:
                y3--;
                break;
        }
    }
}
