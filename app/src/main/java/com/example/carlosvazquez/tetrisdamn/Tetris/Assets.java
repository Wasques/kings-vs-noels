package com.example.carlosvazquez.tetrisdamn.Tetris;

import com.example.carlosvazquez.tetrisdamn.framework.Pixmap;

/**
 * Created by carlos.vazquez on 21/12/2016.
 */
public class Assets {
    public static Pixmap background;
    public static Pixmap cube;
    public static Pixmap king;
    public static Pixmap lft;
    public static Pixmap rght;
    public static Pixmap up;
    public static Pixmap noel;
    public static Pixmap menu;
    public static Pixmap quit;
    public static Pixmap resume;
    public static Pixmap GO;
    public static Pixmap pause;
    public static Pixmap winner;
    public static Pixmap title;
}
