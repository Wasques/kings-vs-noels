package com.example.carlosvazquez.tetrisdamn.Tetris;

import java.util.Random;

/**
 * Created by carlos.vazquez on 21/12/2016.
 */
public class Board {
    static final int WORLD_WIDTH = 10;
    static final int WORLD_HEIGHT = 13;
    static final int SCORE_INCREMENT = 10;
    static final float TICK_INITIAL = 0.5f;
    static final float TICK_DECREMENT = 0.05f;

    int map[][] = new int[WORLD_WIDTH][WORLD_HEIGHT];
    Noels arrayNoels[] = new Noels[11];
    int kingNumber = 1;
    public King currentKing;
    public Noels aNoel;
    public int score = 0;
    float tickTime = 0;
    float tick = TICK_INITIAL;
    public boolean gameOver = false;
    public boolean win = false;

    public Board(){
        newKing();
        newNoel();
    }

    public void newKing(){
        currentKing = new King();
    }

    public void newNoel(){
        for(int i = 1; i < 12; i++) {
            aNoel = new Noels(i);
            arrayNoels[i-1] = aNoel;
        }
    }

    public void update(float deltaTime) {
        tickTime += deltaTime;
        while (tickTime > tick) {
            tickTime -= tick;
            translateNoel();
            if(checkXOC() == true){ gameOver = true;}
            if(currentKing.y == 0) { kingNumber = 2;}
            if(currentKing.y2 == 0){kingNumber = 3;}
            if (currentKing.y3 == 0){win = true;}
        }
    }

    public boolean checkXOC()
    {
        boolean chok = false;
        for(int i = 0; i < 12; i++) {
            if (currentKing.x == arrayNoels[i].x && currentKing.y == arrayNoels[i].y) {
                chok = true;
            }else{chok = false;}
        }
        return chok;
    }

    public void translateNoel(){
        for(int i = 0; i < 12; i++)
        {
            if(i%3 == 0)
            {
                if(arrayNoels[i].y == -1)
                {
                    arrayNoels[i].y = WORLD_WIDTH;
                }
            }else{
                if(arrayNoels[i].y == 11)
                {
                    arrayNoels[i].y = 0;
                }
            }
        }
    }


}

