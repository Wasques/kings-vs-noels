package com.example.carlosvazquez.tetrisdamn.Tetris;

import com.example.carlosvazquez.tetrisdamn.framework.Game;
import com.example.carlosvazquez.tetrisdamn.framework.Graphics;
import com.example.carlosvazquez.tetrisdamn.framework.Pixmap;
import com.example.carlosvazquez.tetrisdamn.framework.Screen;
import android.graphics.Color;
import com.example.carlosvazquez.tetrisdamn.framework.Input.TouchEvent;
import java.util.List;

/**
 * Created by carlos.vazquez on 21/12/2016.
 */
public class GameScreen extends Screen {

    enum Gamestate{

        Paused,
        Running,
        GameOver,
        WIN

    }

    Board board;
    Gamestate state = Gamestate.Running;

    public GameScreen(Game game) {
        super(game);
        board = new Board();
    }

    @Override
    public void update(float deltaTime) {
        List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        game.getInput().getKeyEvents();
        if(state == Gamestate.Running) {
            updateRunning(touchEvents, deltaTime);
        }
        if(state == Gamestate.Paused){
            updatePaused(touchEvents);
        }
        if(state == Gamestate.GameOver)
        {
            updateGO(touchEvents);
        }
        if(state == Gamestate.WIN)
        {
            updateGO(touchEvents);
        }

    }

    private void updateRunning(List<TouchEvent> touchEvents, float deltaTime) {
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_DOWN) {
                if (event.x < 32 && event.y > 32)
                {
                    state = Gamestate.Paused;
                }
                if(event.x < 64 && event.y > 416) {
                    board.currentKing.turnLeft(board.kingNumber);
                }
                if(event.x > 256 && event.y > 416) {
                    board.currentKing.turnRight(board.kingNumber);
                }
                if(event.x > 64 && event.x < 256 && event.y> 416){
                    board.currentKing.goUp(board.kingNumber);
                }
            }
        }
        for(int z = 0; z < 11; z++) {
            if(z%3 == 0) {
                board.arrayNoels[z].y -= 1;
            }else{
                board.arrayNoels[z].y += 1;
            }
        }

        board.update(deltaTime);

        if(board.gameOver){
            state = Gamestate.GameOver;
        }

    }

    private  void updateGO(List<TouchEvent> touchEvents){
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
                if(event.x >= 0 && event.y >= 0) {
                    game.setScreen(new MainMenu(game));
                    return;
                }
            }
        }
    }

    private void updatePaused(List<TouchEvent> touchEvents) {
        int len = touchEvents.size();
        for(int i = 0; i < len; i++) {
            TouchEvent event = touchEvents.get(i);
            if(event.type == TouchEvent.TOUCH_UP) {
                if(event.x > 80 && event.x <= 240) {
                    if(event.y > 100 && event.y <= 148) {
                        state = Gamestate.Running;
                        return;
                    }
                    if(event.y > 150 && event.y < 198) {
                        game.setScreen(new MainMenu(game));
                        return;
                    }
                }
            }
        }
    }

    @Override
    public void render(float deltaTime) {
        Graphics g = game.getGraphics();
        g.drawPixmap(Assets.background, 0, 0);

        drawWorld(board);

        if(state == Gamestate.Running) {
            drawRunningUI();
        }
        if(state == Gamestate.Paused){
            drawPausedUI();
        }
        if(state == Gamestate.GameOver){
            drawGOUI();
        }
        if(state == Gamestate.WIN){
            drawWINUI();
        }

    }

    private void drawWorld(Board board) {
        Graphics g = game.getGraphics();
        King king = board.currentKing;

        int x = king.x * 32;
        int y = king.y * 32;
        int x2 = king.x2 * 32;
        int y2 = king.y2 * 32;
        int x3 = king.x3 * 32;
        int y3 = king.y3 * 32;

        for(int i = 0; i < 11; i++) {
            int xN = board.arrayNoels[i].x * 32;
            int yN = board.arrayNoels[i].y * 32;
            g.drawPixmap(Assets.noel, xN, yN);
        }

        g.drawPixmap(Assets.king, x, y);
        g.drawPixmap(Assets.king, x2, y2);
        g.drawPixmap(Assets.king, x3, y3);

    }

    public void drawRunningUI (){
        Graphics g = game.getGraphics();

        g.drawLine(0, 416, 480, 416, Color.WHITE);
        g.drawPixmap(Assets.lft, 0, 416);
        g.drawPixmap(Assets.rght, 256, 416);
        g.drawPixmap(Assets.up, 128, 416);
        g.drawPixmap(Assets.pause, 0, 0);
    }
    public void drawPausedUI (){
        Graphics g = game.getGraphics();

        g.drawPixmap(Assets.resume, 320/2 - 24, 100);
        g.drawPixmap(Assets.quit, 320/2 - 24, 150);
    }

    public void drawGOUI (){
        Graphics g = game.getGraphics();

        g.drawPixmap(Assets.GO, 0, 480/2);
    }

    public void drawWINUI (){
        Graphics g = game.getGraphics();

        g.drawPixmap(Assets.winner, 50, 200);
    }


    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
