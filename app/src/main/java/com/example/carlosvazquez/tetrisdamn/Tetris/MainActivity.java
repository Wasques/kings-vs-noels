package com.example.carlosvazquez.tetrisdamn.Tetris;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.carlosvazquez.tetrisdamn.R;
import com.example.carlosvazquez.tetrisdamn.framework.Screen;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }
}
