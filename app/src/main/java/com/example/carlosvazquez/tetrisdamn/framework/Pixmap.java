package com.example.carlosvazquez.tetrisdamn.framework;

import com.example.carlosvazquez.tetrisdamn.framework.Graphics.PixmapFormat;

public interface Pixmap {
    public int getWidth();

    public int getHeight();

    public PixmapFormat getFormat();

    public void dispose();
}
